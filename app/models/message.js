import DS from 'ember-data';

export default DS.Model.extend({
    user: DS.attr('string')
    , text:  DS.attr('string')
    , sent: DS.attr('boolean')
    , isChatOwner: DS.attr('boolean')
    , hashConversation: DS.attr('string')
    , createdAt:  DS.attr('date', {
        defaultValue() { return new Date(); }
    })
});
