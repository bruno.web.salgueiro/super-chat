import DS from 'ember-data';
import { v4 } from 'ember-uuid';

/*To-do: Criar controle de usuários, para identificar o proprietário da mensagem 
e os respectivos participantes da conversa.*/
export default DS.Model.extend({
  init() {
    this.setProperties({
      users: []
      , messages: []
      ,'hash': v4()
      , active: true
    })
  },
  users: DS.hasMany('user'),
  messages: DS.hasMany('message'),
  hash: DS.attr('string'),
  active: DS.attr('boolean')
});