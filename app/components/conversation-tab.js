import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
    conversations: computed.alias('conversation.items'),

    actions: {
        activate(hash) {
            this.get('conversation').deactivate();
            this.get('conversation').activateByHash(hash);
        },
        close(item) {
            this.get('conversation').remove(item);
            this.get('conversation').activate();
        }
    }
});
