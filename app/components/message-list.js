import Ember from 'ember';
import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
    websockets: Ember.inject.service(),
    socketRef: null,

    messages: computed.alias('conversation.active.firstObject.messages'),

    didInsertElement() {
        this._super(...arguments);
        // To-do: Setar endpoint de websocket para suportar Long Pooling.
        const socket = this.get('websockets').socketFor('ws://localhost:7200');

        socket.on('open', this.openHandler, this);
        socket.on('message', this.messageHandler, this);
        socket.on('close', this.closeHandler, this);

        this.set('socketRef', socket);
    },

    willDestroyElement() {
        this._super(...arguments);

        const socket = this.get('socketRef');

        socket.off('open', this.openHandler);
        socket.off('message', this.messageHandler);
        socket.off('close', this.closeHandler);
    },

    openHandler(event) {
        console.log(`On open event has been called: ${event}`);        
    },

    messageHandler(event) {
        this.get('conversation').addMessage(event.message);
    },

    closeHandler(event) {
        console.log(`On close event has been called: ${event}`);
    }
});
