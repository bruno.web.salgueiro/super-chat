import Component from '@ember/component';

export default Component.extend({
    actions: {
        newTab() {
            const conversation = this.get('store').createRecord('conversation');

            this.get('conversation').deactivate();
            this.get('conversation').add(conversation);
        },
        closeTabs() {
            this.get('conversation').empty();
        }
    }
});
