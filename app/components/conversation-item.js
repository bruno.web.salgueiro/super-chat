import Ember from 'ember';
import Component from '@ember/component';

const { $ } = Ember;

export default Component.extend({
    didRender: function() {
        const messages = $(".messages");

        if (messages.length)
            messages.scrollTop(messages[0].scrollHeight);
    },

    registerMessage: function(text) {
        const random = Math.round(Math.random());

        /*To-do: Fazer com que a propriedade "sent", tenha um comportamento de Enum,
            para controlar o estado da mensagem, assim posso controlar 
            se a mensagem foi de fato registrada no servidor e/ou se chegou ao 
            meu destinatário com sucesso.
        */
        const message = this.get('store').createRecord('message', {
            user: 'Guest'
            , text: text
            , sent: false
            , isChatOwner: !!random
            , hashConversation: this.get('conversation.active.firstObject.hash')
        });

        this.get('conversation').addMessage(message);
        
        message.save().then(this.setMessageState).catch(this.failure);
    },

    setMessageState: function(message) {
        //To-do: Setar estado da mensagem.
        console.log(message);
    },

    failure: function(event) {
        //To-do: Setar estado da mensagem e tratar mensagem erro.
        console.log(event.message);
    },

    clearSender: function () {
        this.set('message', '');
    },

    actions: {
        send() {
            let text = this.get('message');
            
            if (text)
                this.registerMessage(text);

            this.clearSender();
        }
    }
});
