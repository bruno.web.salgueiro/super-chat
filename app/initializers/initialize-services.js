export function initialize(application) {
  application.inject('component', 'conversation', 'service:conversation');
  application.inject('component', 'store', 'service:store');
}

export default {
  initialize
};
