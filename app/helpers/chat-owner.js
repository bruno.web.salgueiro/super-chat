import Ember from 'ember';
import { helper } from '@ember/component/helper';

export function chatOwner(message) {
  return Ember.get(message, 'firstObject.isChatOwner') ? 'Você' : Ember.get(message, 'firstObject.user');
}

export default helper(chatOwner);
