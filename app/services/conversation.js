import Ember from 'ember';
import Service from '@ember/service';
import { computed } from '@ember/object';

export default Service.extend({
    store: Ember.inject.service(),

    items: null,

    active: computed.filterBy('items', 'active', true),

    init() {
        this._super(...arguments);
        this.set('items', []);
    },

    activate() {
        const lastItem = this.get('items.lastObject.hash');

        this.activateByHash(lastItem);
    },

    deactivate() {
        this.get('items').setEach('active', false);
    },

    activateByHash(hash) {
        Ember.set(this.getByHash(hash), 'active', true)
    },

    getByHash(hash) {
        return this.get('items').findBy('hash', hash);
    },

    add(item) {
        this.get('items').pushObject(item);
    },

    addMessage(message){
        const hash = message.get('hashConversation');

        this.getByHash(hash).get('messages').pushObject(message);
    },

    remove(item) {
        this.get('items').removeObject(item);
    },

    empty() {
        this.get('items').clear();
    }
});
