import Ember from 'ember';
import { moduleFor, test } from 'ember-qunit';

const conversationList = [
  {
    hash: "dad93c88-51fd-4753-9419-afbcaf12c7ba"
    , messages: []
    , active: false
  },
  {
    hash: "bas93c77-51fd-4753-9419-afbcaf12c7ba"
    , messages: ["testes"]
    , active: false
  }
];

moduleFor('service:conversation', 'Unit | Service | conversation', {
  // Specify the other units that are required for this test.
  // needs: ['service:foo']
});

test('should activate conversation', function(assert) {
  let service = this.subject();
  
  service.items = conversationList;
  service.activate();

  assert.ok(service.items[1].active);
});

test('should deactivate conversation', function(assert) {
  let service = this.subject();
  
  service.items = conversationList;
  service.activate();
  service.deactivate();

  assert.ok(!service.items[1].active);
});

test('should activate conversation by hash', function(assert) {
  let service = this.subject();
  let hash = "dad93c88-51fd-4753-9419-afbcaf12c7ba";

  service.items = conversationList;
  
  service.activateByHash(hash);

  assert.equal(service.get('active.firstObject.hash'), hash);
});

test('should return conversation by hash', function(assert) {
  let service = this.subject();
  
  service.items = conversationList;
  const conversation = service.getByHash("dad93c88-51fd-4753-9419-afbcaf12c7ba");
  
  assert.ok(conversation);
});

test('should add conversation', function(assert) {
  let service = this.subject();

  service.add({
    hash: "dad93c88-51fd-4753-9419-afbcaf12c7ba"
    , messages: []
    , active: true
  });

  assert.ok(service && service.items.length > 0);
});

test('should add message to conversation', function(assert) {
  let service = this.subject();

  service.add(Ember.Object.create({
    hash: "dad93c88-51fd-4753-9419-afbcaf12c7ba"
    , messages: []
    , active: true
  }));

  service.addMessage(Ember.Object.create({
    user: 'Guest',
    text: 'test message',
    isChatOwner: true,
    hashConversation: "dad93c88-51fd-4753-9419-afbcaf12c7ba"
  }));

  assert.ok(service.items[0].messages.length > 0);
});

test('should remove conversation', function(assert) {
  let service = this.subject();
  
  service.items = conversationList;
  service.remove(service.items[0]);
  
  assert.ok(service.items.length < 2);
});


test('should empty conversation', function(assert) {
  let service = this.subject();
  
  service.items = conversationList;
  service.empty();
  
  assert.equal(service.items.length, 0);
});
