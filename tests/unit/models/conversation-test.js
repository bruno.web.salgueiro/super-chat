import Ember from 'ember';
import { moduleForModel, test } from 'ember-qunit';

moduleForModel('conversation', 'Unit | Model | conversation', {
  // Specify the other units that are required for this test.
  needs: ['model:user']
});

test('it has relationship with users', function(assert) {
  let store = this.store();

  // const User = store.modelFor('user');
  const Conversation = store.modelFor('conversation');
  const relationship = Ember.get(Conversation, 'relationshipsByName').get('users');

  assert.equal(relationship.key, 'users', 'has many users');
});
