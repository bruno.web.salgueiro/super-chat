import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('empty-conversation', 'Integration | Component | empty conversation', {
  integration: true
});

test('it renders', function(assert) {

  this.render(hbs`{{empty-conversation}}`);

  assert.equal(this.$().text().trim(), 'Você ainda não iniciou nenhuma conversa.');

});
