import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('message-list', 'Integration | Component | message list', {
  integration: true
});

test('it renders', function(assert) {

  this.render(hbs`{{message-list}}`);

  assert.equal(this.$().text().trim(), 'Envie uma mensagem para começar a conversa...');

});
