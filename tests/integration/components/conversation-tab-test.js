import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('conversation-tab', 'Integration | Component | conversation tab', {
  integration: true
});

test('it renders', function(assert) {

  this.render(hbs`{{conversation-tab}}`);

  assert.equal(this.$().text().trim(), '');

});
