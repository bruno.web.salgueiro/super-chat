import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('empty-messages', 'Integration | Component | empty messages', {
  integration: true
});

test('it renders', function(assert) {

  this.render(hbs`{{empty-messages}}`);

  assert.equal(this.$().text().trim(), 'Envie uma mensagem para começar a conversa...');

});
