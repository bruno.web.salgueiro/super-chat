// import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

// const conversation = Ember.Service.extend({
//   items: [{
//     hash: "dad93c88-51fd-4753-9419-afbcaf12c7ba"
//     , messages: ["teste"]
//     , active: true
//   }]
// });

moduleForComponent('conversation-item', 'Integration | Component | conversation item', {
  integration: true,

  // beforeEach: function () {
  //   this.register('service:conversation', conversation);
  //   this.inject.service('conversation', { as: 'conversation' });
  // }
});

test('it renders', function(assert) {
  this.render(hbs`{{conversation-item}}`);

  assert.equal(this.$().text().trim(), 'Você ainda não iniciou nenhuma conversa.');
});

// test('it renders with conversation', function(assert) {

//   this.render(hbs`{{conversation-item}}`);

//   assert.equal(this.$().text().trim(), 'Você não iniciou nenhuma conversa.');
// });

