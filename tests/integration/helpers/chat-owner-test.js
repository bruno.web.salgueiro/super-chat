
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('chat-owner', 'helper:chat-owner', {
  integration: true
});

test('it renders owner', function(assert) {
  const message = {
    "user": "Você"
    ,"text": "teste"
    ,"sent": false
    ,"isChatOwner": true
    ,"hashConversation": "d0636778-b774-4346-b056-3364ac8017e4"
    ,"createdAt": new Date()
  };

  this.set('inputValue', message);

  this.render(hbs`{{chat-owner inputValue}}`);

  assert.equal(this.$().text().trim(), 'Você');
});

test('it renders guest', function(assert) {
  const message = {
    "user": "Guest"
    ,"text": "teste"
    ,"sent": false
    ,"isChatOwner": false
    ,"hashConversation": "d0636778-b774-4346-b056-3364ac8017e4"
    ,"createdAt": new Date()
  };

  this.set('inputValue', message);

  this.render(hbs`{{chat-owner inputValue}}`);

  assert.equal(this.$().text().trim(), 'Guest');
});

