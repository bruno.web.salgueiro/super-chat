# super-chat

Este README detalha como deve ser a instalação e execução do projeto.
Segue os passos.

## Pré-requisitos

Você precisa destes software para rodar a aplicação no seu computador.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with NPM)
* [Ember CLI](https://ember-cli.com/)

## Instalação

* `git clone git@gitlab.com:bruno.web.salgueiro/super-chat.git` this repository
* `cd super-chat`
* `npm install`

## Rodando / Development

* `ember serve`
* Visit seu app em [http://localhost:4200](http://localhost:4200).
* Visit os testes em [http://localhost:4200/tests](http://localhost:4200/tests).

### Rodando Testes

Foram criados alguns testes básicos a critériode demonstração principalmente com foco no service de conversation.

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Nesse exemplo o deploy será feito usando AWS S3, para realizar o deploy é necessário configurar as chaves {KEY,SECRET} do seu IAM (AWS) em um arquivo ".env" na raíz do projeto, como estou usando recursos privados, não será disponibilizado as chaves de acesso neste repositório.
Deploy production gera o build otimizado.

* `ember deploy` (development)
* `ember deploy --environment production` (production) 

## Arquitetura da solução

A ideia principal é concentrar os chats em um recurso singleton onde é possível armazenar objetos do tipo "Conversation" estes que possuem relação com uma lista de usuários e uma lista mensagens.

O recurso Singleton utilizado para "Conversation" é um service, que irá armazenar todos os chats abertos onde cada chat é um objeto "Conversation" que deve conter seu identificador único "hash", esse identificador é importante para gerenciar as mensagens enviadas pelos usuários do grupo no caso de utilizarmos um banco não relacional. 

Estas mensagens quando adicionados a uma conversa, são adicionados inMemory na aplicação e enviados ao servidor onde as mensagens poderão ser registradas e notificadas via websocket que foi o recurso escolhido para realizar as notificações via Long Polling a todos os usuários conectados.

Observações: 
    O recurso de registrar mensagens, realiza um XMLHttpRequest Fake, pois não temos endpoint configurado.
    O recurso de websocket também foi implementado de forma Fake para simular a implementação.
    O sistema de usuários utiliza um recurso random somente para simular uma conversa entre 2 usuários.
