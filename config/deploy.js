/* eslint-env node */
'use strict';

module.exports = function(deployTarget) {
  let ENV = {
    's3': {
      accessKeyId: process.env.AWS_KEY,
      secretAccessKey: process.env.AWS_SECRET,
      bucket: 'creditas-test',
      filePattern: '**/*.{js,css,png,gif,ico,jpg,map,xml,txt,svg,swf,eot,ttf,woff,woff2,otf,html}',
      region: 'sa-east-1'
    }
  };

  return ENV;
};
